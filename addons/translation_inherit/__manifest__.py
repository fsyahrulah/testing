# -*- coding: utf-8 -*-
{
    'name': "translation_inherit",

    'summary': """
        A Module for fir translation""",

    'description': """
         A Module for fir translation
    """,

    'author': "ERP",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Translation',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['stock', 'sale_purchase', 'sale_stock', 'purchase_stock'],

    # always loaded
    'data': [
        'data/stock_data.xml',
        'views/stock_picking_type.xml',
    ],
    'installable': True,
    'auto_install': False,

    # only loaded in demonstration mode
}
