from odoo import api, models, fields

class PickingType(models.Model):
    _inherit = "stock.picking.type"

    code = fields.Selection(selection_add=[('return', 'Return')])


class ResCompany(models.Model):
    _inherit = 'res.company'

    # Create Sequences
    # -------------------------------------------------------------------------
    def _create_return_sequence(self):
        return_vals = []
        for company in self:
            return_vals.append({
                'name': 'Return (%s)' % company.name,
                'code': 'stock.return',
                'company_id': company.id,
                'prefix': 'RT/',
                'padding': 5,
            })
        if return_vals:
            self.env['ir.sequence'].create(return_vals)

    @api.model
    def create_missing_return_sequence(self):
        company_ids = self.env['res.company'].search([])
        company_has_return_seq = self.env['ir.sequence'].search([('code', '=', 'stock.return')]).mapped('company_id')
        company_todo_sequence = company_ids - company_has_return_seq
        company_todo_sequence._create_return_sequence()

    def _create_per_company_sequences(self):
        super(ResCompany, self)._create_per_company_sequences()
        self._create_return_sequence()

    # Create Picking types
    # -------------------------------------------------------------------------
    def _create_return_picking_type(self):
        return_vals = []
        for company in self:
            sequence = self.env['ir.sequence'].search([
                ('code', '=', 'stock.return'),
                ('company_id', '=', company.id),
            ])
            return_vals.append({
                'name': 'Return',
                'company_id': company.id,
                'warehouse_id': False,
                'sequence_id': sequence.id,
                'code': 'return',
                'default_location_src_id': self.env.ref('stock.stock_location_suppliers').id,
                'default_location_dest_id': self.env.ref('stock.stock_location_customers').id,
                'sequence_code': 'RT',
            })
        if return_vals:
            self.env['stock.picking.type'].create(return_vals)

    @api.model
    def create_missing_return_picking_type(self):
        company_ids = self.env['res.company'].search([])
        company_has_return_picking_type = (
            self.env['stock.picking.type']
            .search([
                ('default_location_src_id.usage', '=', 'supplier'),
                ('default_location_dest_id.usage', '=', 'customer'),
            ])
            .mapped('company_id')
        )
        company_todo_picking_type = company_ids - company_has_return_picking_type
        company_todo_picking_type._create_return_picking_type()

    def _create_per_company_picking_types(self):
        super(ResCompany, self)._create_per_company_picking_types()
        self._create_return_picking_type()


    # Create Stock rules
    # -------------------------------------------------------------------------
    def _create_return_rule(self):
        return_route = self.env.ref('translation_inherit.route_return_shipping')
        supplier_location = self.env.ref('stock.stock_location_suppliers')
        customer_location = self.env.ref('stock.stock_location_customers')

        return_vals = []
        for company in self:
            return_picking_type = self.env['stock.picking.type'].search([
                ('company_id', '=', company.id),
                ('default_location_src_id.usage', '=', 'supplier'),
                ('default_location_dest_id.usage', '=', 'customer'),
            ], limit=1, order='sequence')
            return_vals.append({
                'name': '%s → %s' % (supplier_location.name, customer_location.name),
                'action': 'pull',
                'location_id': customer_location.id,
                'location_src_id': supplier_location.id,
                'procure_method': 'make_to_stock',
                'route_id': return_route.id,
                'picking_type_id': return_picking_type.id,
                'company_id': company.id,
            })
        if return_vals:
            self.env['stock.rule'].create(return_vals)

    @api.model
    def create_missing_return_rule(self):
        return_route = self.env.ref('translation_inherit.route_return_shipping')

        company_ids = self.env['res.company'].search([])
        company_has_return_rule = self.env['stock.rule'].search([('route_id', '=', return_route.id)]).mapped('company_id')
        company_todo_rule = company_ids - company_has_return_rule
        company_todo_rule._create_return_rule()

    def _create_per_company_rules(self):
        super(ResCompany, self)._create_per_company_rules()
        self._create_return_rule()


class Users(models.Model):
    _inherit = 'res.users'

    def write(self, vals):
        if 'lang' in vals:
            stock_picking = self.env['stock.picking.type'].sudo().search([])
            for line in stock_picking:
                ir_translation = self.env['ir.translation'].sudo().search(
                    [('name', '=', 'stock.picking.type,name'), ('lang', '=', vals['lang']), ('res_id', '=', line.id)])
                if not ir_translation:
                    source_translated = self.env['ir.translation'].sudo().search(
                        [('name', '=', 'stock.picking.type,name'),
                         ('state', '=', 'translated'),
                         ('value', '=', line.name)], limit=1).src
                    print("sumber", line.name, source_translated)
                    if source_translated:
                        translated = self.env['ir.translation'].sudo().search(
                            [('name', '=', 'stock.picking.type,name'), ('lang', '=', vals['lang']),
                             ('state', '=', 'translated'), ('src', '=', source_translated)], limit=1)
                    else:
                        translated = self.env['ir.translation'].sudo().search(
                            [('name', '=', 'stock.picking.type,name'), ('lang', '=', vals['lang']),
                             ('state', '=', 'translated'), ('src', '=', line.name)], limit=1)
                    print("translted to", translated)
                    if translated:
                        self.env['ir.translation'].sudo().create({
                            'name': 'stock.picking.type,name',
                            'res_id': line.id,
                            'lang': vals['lang'],
                            'type': 'model',
                            'src': translated.src,
                            'value': translated.value,
                            'module': translated.module,
                            'state': translated.state
                        })
                    else:
                        if vals['lang'] == 'en_US':
                            self.env['ir.translation'].sudo().create({
                                'name': 'stock.picking.type,name',
                                'res_id': line.id,
                                'lang': 'en_US',
                                'type': 'model',
                                'src': line.name,
                                'value': source_translated,
                                'module': 'stock',
                                'state': 'translated'
                            })

        return super(Users, self).write(vals)
#
class BaseLanguageInstall(models.TransientModel):
    """ Install Language"""
    _inherit = "base.language.install"

    def lang_install(self):
        action = super(BaseLanguageInstall, self).lang_install()
        stock_picking = self.env['stock.picking.type'].sudo().search([])
        for line in stock_picking:
            ir_translation = self.env['ir.translation'].sudo().search(
                [('name', '=', 'stock.picking.type,name'), ('lang', '=', self.lang), ('res_id', '=', line.id)])
            if not ir_translation:
                source_translated = self.env['ir.translation'].sudo().search(
                    [('name', '=', 'stock.picking.type,name'), ('lang', '=', self.env.lang), ('state', '=', 'translated'),
                     ('value', '=', line.name)], limit=1).src
                if source_translated:
                    translated = self.env['ir.translation'].sudo().search(
                        [('name', '=', 'stock.picking.type,name'), ('lang', '=', self.lang), ('state', '=', 'translated'), ('src', '=', source_translated)], limit=1)
                else:
                    translated = self.env['ir.translation'].sudo().search(
                        [('name', '=', 'stock.picking.type,name'), ('lang', '=', self.lang),
                         ('state', '=', 'translated'), ('src', '=', line.name)], limit=1)
                if translated:
                    self.env['ir.translation'].sudo().create({
                        'name': 'stock.picking.type,name',
                        'res_id': line.id,
                        'lang': self.lang,
                        'type': 'model',
                        'src': translated.src,
                        'value': translated.value,
                        'module': translated.module,
                        'state': translated.state
                    })

        return action
